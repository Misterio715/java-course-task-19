package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.api.service.IProjectTaskService;
import org.fmavlyutov.exception.entity.ProjectNotFoundException;
import org.fmavlyutov.exception.entity.TaskNotFoundException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) {
            throw new InvalidOrEmptyIdException("Invalid or empty project id!");
        }
        if (taskId == null || taskId.isEmpty()) {
            throw new InvalidOrEmptyIdException("Invalid or empty task id!");
        }
        if (!projectRepository.existsById(projectId)) {
            throw new ProjectNotFoundException();
        }
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) {
            throw new InvalidOrEmptyIdException("Invalid or empty project id!");
        }
        if (taskId == null || taskId.isEmpty()) {
            throw new InvalidOrEmptyIdException("Invalid or empty task id!");
        }
        if (!projectRepository.existsById(projectId)) {
            throw new ProjectNotFoundException();
        }
        final Task task = taskRepository.findOneById(taskId);
        if (task == null ) {
            throw new TaskNotFoundException();
        }
        task.setProjectId(null);;
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) {
            taskRepository.removeById(task.getId());
        }
        projectRepository.removeById(projectId);
    }

}
