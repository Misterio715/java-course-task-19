package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.api.service.ITaskService;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.exception.entity.TaskNotFoundException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIndexException;
import org.fmavlyutov.exception.field.InvalidOrEmptyNameException;
import org.fmavlyutov.model.Task;

import java.util.Collections;
import java.util.List;

public class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Task task = new Task();
        task.setName(name);
        if (description != null) {
            task.setDescription(description);
        }
        return add(task);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        if (name == null | name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Task task = findOneById(id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        if (name == null | name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Task task = findOneByIndex(index);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        final Task task = repository.findOneById(id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        final Task task = repository.findOneByIndex(index);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(String id) {
        if (id == null || id.isEmpty()) {
            return Collections.emptyList();
        }
        return repository.findAllByProjectId(id);
    }

}
