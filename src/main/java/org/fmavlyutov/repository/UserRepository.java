package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.IUserRepository;
import org.fmavlyutov.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findOneByLogin(final String login) {
        for (final User user : records) {
            if (login.equals(user.getLogin())) {
                return user;
            }
        }
        return null;
    }

    @Override
    public User findOneByEmail(final String email) {
        for (final User user : records) {
            if (email.equals(user.getEmail())) {
                return user;
            }
        }
        return null;
    }

    @Override
    public Boolean isLoginExists(final String login) {
        for (final User user : records) {
            if (login.equals(user.getLogin())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean isEmailExists(final String email) {
        for (final User user : records) {
            if (email.equals(user.getEmail())) {
                return true;
            }
        }
        return false;
    }

}
