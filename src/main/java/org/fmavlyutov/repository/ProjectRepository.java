package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.model.Project;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
}
