package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.IRepository;
import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> records = new ArrayList<>();

    @Override
    public M add(final M model) {
        records.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return records;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(records);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(final Sort sort) {
        return findAll(sort.getComparator());
    }

    @Override
    public M findOneById(final String id) {
        for (final M model : records) {
            if (id.equals(model.getId())) {
                return model;
            }
        }
        return null;
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return records.get(index);
    }

    @Override
    public M remove(final M model) {
        records.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) {
        final M model = findOneById(id);
        if (model == null) {
            return null;
        }
        return remove(model);
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) {
            return null;
        }
        return remove(model);
    }

    @Override
    public void clear() {
        records.clear();
    }

    @Override
    public Integer getSize() {
        return records.size();
    }

    @Override
    public Boolean existsById(final String id) {
        return findOneById(id) != null;
    }

}
