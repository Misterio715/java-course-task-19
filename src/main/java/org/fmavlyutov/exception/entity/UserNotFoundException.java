package org.fmavlyutov.exception.entity;

public final class UserNotFoundException extends AbstractEntityException {

    public UserNotFoundException() {
        super("User not found!");
    }
}
