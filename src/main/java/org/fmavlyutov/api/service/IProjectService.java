package org.fmavlyutov.api.service;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.exception.field.InvalidOrEmptyNameException;
import org.fmavlyutov.model.Project;

public interface IProjectService extends IService<Project> {

    Project create(String name, String description) throws InvalidOrEmptyNameException;

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

}
