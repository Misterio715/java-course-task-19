package org.fmavlyutov.api.service;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    List<Task> findAllByProjectId(String id);

}
