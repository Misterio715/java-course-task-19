package org.fmavlyutov.api.repository;

import org.fmavlyutov.model.Project;

public interface IProjectRepository extends IRepository<Project> {
}
