package org.fmavlyutov.api.repository;

import org.fmavlyutov.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String id);

}
