package org.fmavlyutov.command.project;

import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "show project by index";
    }

    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        showProject(project);
    }

}
