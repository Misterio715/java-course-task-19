package org.fmavlyutov.command.system;

import org.fmavlyutov.command.AbstractCommand;

public final class SystemArgumentListCommand extends AbstractSystemCommand {


    @Override
    public String getArgument() {
        return "-arg";
    }

    @Override
    public String getDescription() {
        return "display all commands";
    }

    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : getCommandService().getTerminalCommands()) {
            if (command == null) {
                continue;
            }
            String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) {
                continue;
            }
            System.out.println(argument);
        }
        System.out.println();
    }

}
