package org.fmavlyutov.command.system;

import org.fmavlyutov.command.AbstractCommand;

public final class SystemHelpCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "display info about commands and arguments";
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : getCommandService().getTerminalCommands()) {
            System.out.println(command);
        }
        System.out.println();
    }

}
