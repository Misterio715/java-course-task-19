package org.fmavlyutov.command.task;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "complete task by id";
    }

    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().changeStatusById(id, Status.COMPLETED);
        showTask(task);
    }

}
