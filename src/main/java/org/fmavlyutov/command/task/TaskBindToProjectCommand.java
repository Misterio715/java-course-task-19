package org.fmavlyutov.command.task;

import org.fmavlyutov.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "bind task to project";
    }

    @Override
    public String getName() {
        return "task-bind-to-project";
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("Enter project id: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id: ");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(projectId, taskId);
    }

}
