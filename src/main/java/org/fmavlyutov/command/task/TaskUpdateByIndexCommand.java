package org.fmavlyutov.command.task;

import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "update task by index";
    }

    @Override
    public String getName() {
        return "task-update-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Task task = getTaskService().updateByIndex(index, name, description);
        showTask(task);
    }

}
