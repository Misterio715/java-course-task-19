package org.fmavlyutov.command.task;

import org.fmavlyutov.util.TerminalUtil;

public final class TaskUnbindToProjectCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "unbind task from project";
    }

    @Override
    public String getName() {
        return "task-unbind-from-project";
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("Enter project id: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id: ");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(projectId, taskId);
    }

}
