package org.fmavlyutov.command.user;

import org.fmavlyutov.model.User;
import org.fmavlyutov.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "registry user";
    }

    @Override
    public String getName() {
        return "user-registry";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY USER]");
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        final String email = TerminalUtil.nextLine();
        final User user = getAuthService().registry(login, password, email);
        showUser(user);
    }

}
